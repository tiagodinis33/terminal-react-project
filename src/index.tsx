
import blessed from 'blessed';
import { render } from 'react-blessed';
import App from "./components/app"
import React from 'react';
// @ts-ignore
import packageJson from "../package.json"
// Creating our screen
const screen = blessed.screen({
    title: packageJson.name,
    smartCSR: true
});
// Adding a way to quit the program
screen.key(['escape', 'q', 'C-c'], function (ch, key) {
    return process.exit(0);
});
// Rendering the React app using our screen
render(<App/>, screen);