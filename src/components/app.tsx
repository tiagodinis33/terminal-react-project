import { Widgets } from "blessed";
import React, { useState } from "react";
import { KeyPressEventHandler } from "react-blessed";
function Center(props: any) {
    return <text top="center" left="center" style={props.style}>{props.children}</text>
}
const color = "#202020"
const style = { bg: color, border: { bg: color } }
const pressedStyle = { bg: "blue", border: { bg: "blue" } }
var gcounter = 0;
export default function App() {
    const [counter, setCounter] = useState(0);
    gcounter = counter
    const [clicked, setClicked] = useState(false)
    return (
        <>
            <box bg="#0000FF"></box>
            <box
                width="30%"
                height="60%"
                border={{ type: 'line' }}
                style={style}
                top="center"
                left="center"
                shadow
                tags
            >
                Clicks:
                <button style={clicked? pressedStyle : style} fg="white" top="center" border="line" left="center" shadow={!clicked} mouse width={20} height={3} onClick={a => {
                    setCounter(counter + 1)

                    setTimeout(() => {
                        setCounter(gcounter - 1)
                    }, 1000)
                }}
                onMousedown={a =>{
                    setClicked(true)
                }}
                onMouseup={a =>{
                    setClicked(false)
                }}
                ><Center style={style}>Click Me!</Center></button>
                <progressbar
                    keys={true}
                    orientation="horizontal"
                    style={{ bar: { bg: "blue" }, ...style }}
                    height={3}
                    position={{ top: 1, bottom: 0, left: 0, right: 0 }}
                    border={{ type: "line", fg: 7 }}
                    filled={(counter / 20) * 100} label={`${counter} cps`}
                />
            </box>
        </>
    )
}